#include <iostream>
using namespace std;

const int n=5;
int i, j;
int used[n];

//  
int graph[n][n] =
{
{0, 1, 0, 0, 1},
{1, 0, 1, 1, 0},
{0, 1, 0, 0, 1},
{0, 1, 0, 0, 1},
{1, 0, 1, 1, 0}
};

//  
void poisk(int st)
{
	int r;
	cout<<st+1<<" ";
	used[st]=1;
	for (r=0; r<=n; r++)
	if ((graph[st][r]!=0) && (!used[r]))
	poisk(r);
}
// 
int main()
{
	int start;
	cout<<"Матрица смежности графа:\n";
	for (i=0; i<n; i++) {
		used[i]=0;
		for (j=0; j<n; j++)
			cout<<" "<<graph[i][j];
		cout<<endl;
	}
	
	cout<<"Стартовая вершина: ";
	cin>>start;
	cout<<"Порядок обхода: ";
	poisk(start-1);
}
